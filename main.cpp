#include <QQmlApplicationEngine>
#include <QGuiApplication>
#include <QQmlContext>
#include <QQuickStyle>
#include <QSettings>
#include <QDebug>

#include "keyemitter.h"

int main(int argc, char *argv[])
{
    QGuiApplication::setApplicationName("Serial Experiments Lain");
    QGuiApplication::setOrganizationName("lainpsxweb.germanaizek");
    QGuiApplication::setApplicationName("lainpsxweb.germanaizek");
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);

    QGuiApplication *app = new QGuiApplication(argc, (char**)argv);
    QQuickStyle::setStyle("Suru");

    QQmlApplicationEngine engine;
    KeyEmitter keyEmitter;
    //engine.rootContext()->setContextProperty("keyEmitter", &keyEmitter);
    engine.rootContext()->setContextProperty("availableStyles", QQuickStyle::availableStyles());
    engine.load(QUrl("qml/Main.qml"));
    if (engine.rootObjects().isEmpty()) {
		return -1;
	}
    return app->exec();
}
