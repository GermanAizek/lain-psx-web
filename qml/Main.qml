import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.5
import QtQuick.Controls.Suru 2.2
import QtQuick.Layouts 1.3
import QtSystemInfo 5.5
import QtWebEngine 1.8

ApplicationWindow  {
    id: window
    width: 360
    height: 520
    visible: true
    title: "Serial Experiments Lain"

    StackView {
        id: stackView
        anchors.fill: parent
    }

    Component.onCompleted: {
        stackView.push("PlayerPage.qml")
        Suru.theme = Suru.Dark
    }

    ScreenSaver {
        screenSaverEnabled: !(Qt.application.active)
    }
}
