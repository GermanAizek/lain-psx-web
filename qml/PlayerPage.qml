import QtQuick 2.7
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3
import QtQuick.Controls 2.5
import QtQuick.Controls.Suru 2.2
import QtQuick.Layouts 1.3
import QtWebEngine 1.8

Page  {
    anchors.fill: parent
    WebEngineView {
        id: webView
        anchors.fill: parent
        url: "https://3d.laingame.net/#/game"
        zoomFactor: 0.8

        settings.accelerated2dCanvasEnabled: true

        Component.onCompleted: {
            webView.runJavaScript("document", function(result) {}) // resize canvas
        }

        profile: WebEngineProfile {
            httpUserAgent: "Mozilla/5.0 (Linux; Android 11; SM-A125U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Mobile Safari/537.36"
        }

        onNewViewRequested : function (request) {
            request.action = WebEngineNavigationRequest.IgnoreRequest
            Qt.openUrlExternally(request.requestedUrl)
        }

        onJavaScriptDialogRequested: (request) => {
            request.accepted = true;
            jsDialog.message = request.message
            jsDialog.accepted.connect(() => request.dialogAccept())
            jsDialog.rejected.connect(() => request.dialogReject())
            jsDialog.open()
        }

        onLoadingChanged: {
            if (loading === false) {
                window.showFullScreen(true)
            }
        }
    }

    Button {
        parent: parent.bottom
        width: 256
        height: 64
        text: "Down"
        focusPolicy: Qt.NoFocus
        onReleased: {
            //keyEmitter.keyPressed(webView, Qt.Key_Down)
            webView.runJavaScript('var kbEvent= document.createEvent("KeyboardEvent");var initMethod = typeof kbEvent.initKeyboardEvent !== "undefined" ? "initKeyboardEvent" : "initKeyEvent";kbEvent[initMethod]("keydown",true,true,window,false,false,false,false,88,0);document.dispatchEvent(kbEvent);', function(result) {})
        }
    }

    Rectangle {
        id: webViewLoadingIndicator
        anchors.fill: parent
        z: 1
        color: Suru.backgroundColor
        visible: webView.loading === true
		
		AnimatedImage {
			anchors.centerIn: parent
			source: "../assets/_spin.gif"
		}

        BusyIndicator {
            id: busy
            anchors.centerIn: parent
        }
    }
}
